package com.foodappui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.foodappui.R;
import com.foodappui.util.UIManager;

public class ForgotActivity extends AppCompatActivity {
    ConstraintLayout lay_forgot;
    AppCompatTextView lb_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        lay_forgot = findViewById(R.id.lay_forgot);
        lb_signin = findViewById(R.id.lb_signin);

        UIManager.set_bottom_sheet(this, lay_forgot, 75);
        UIManager.set_text_decoration(lb_signin, UIManager.get_color_code(this, R.color.tx_light, false, "If you have an account, ") + UIManager.get_color_code(this, R.color.colorPrimary, true, "Sign In"));
        lb_signin.setOnClickListener(v -> startActivity(new Intent(ForgotActivity.this, SigninActivity.class)));
    }
}