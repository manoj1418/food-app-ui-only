package com.foodappui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.foodappui.R;
import com.foodappui.util.UIManager;

public class SigninActivity extends AppCompatActivity {
    ConstraintLayout lay_login;
    AppCompatImageView tx_sign_in;
    AppCompatTextView lb_forgot, lb_otp, lb_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        lay_login = findViewById(R.id.lay_signin);
        tx_sign_in = findViewById(R.id.tx_sign_in);
        lb_forgot = findViewById(R.id.lb_forgot);
        lb_otp = findViewById(R.id.lb_otp);
        lb_signup = findViewById(R.id.lb_signup);

        UIManager.set_bottom_sheet(this, lay_login, 75);
        UIManager.set_text_decoration(lb_forgot, UIManager.get_color_code(this, R.color.colorPrimary, true, "Forgot Password ?"));
        UIManager.set_text_decoration(lb_otp, UIManager.get_color_code(this, R.color.tx_light, false, "Activate account with ") + UIManager.get_color_code(this, R.color.colorPrimary, true, "OTP"));
        UIManager.set_text_decoration(lb_signup, UIManager.get_color_code(this, R.color.tx_light, false, "Not yet a member, ") + UIManager.get_color_code(this, R.color.colorPrimary, true, "Sign up"));

        lay_login.setOnClickListener(v -> startActivity(new Intent(SigninActivity.this, MainActivity.class)));
        lb_forgot.setOnClickListener(v -> startActivity(new Intent(SigninActivity.this, ForgotActivity.class)));
        lb_otp.setOnClickListener(v -> startActivity(new Intent(SigninActivity.this, ForgotActivity.class)));
        lb_signup.setOnClickListener(v -> startActivity(new Intent(SigninActivity.this, SignupActivity.class)));
    }
}