package com.foodappui.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

public class Corner_ImgView extends AppCompatImageView {
    private Path path;
    private RectF rect;

    public Corner_ImgView(Context context) {
        super(context);
        init();
    }

    public Corner_ImgView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Corner_ImgView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        path = new Path();
        rect = new RectF(0, 0, this.getWidth(), this.getHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float radius = 36.0f;
        path.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(path);
        super.onDraw(canvas);
    }
}