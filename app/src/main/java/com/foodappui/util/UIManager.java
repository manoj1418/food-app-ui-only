package com.foodappui.util;

import android.content.Context;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class UIManager {

    public static String get_color_code(Context ctx, int color_id, boolean underline, String text) {
        if (color_id == 0)
            return underline ? "<u>" + text + "</u>" : text;
        else
            return "<font color=#" + Integer.toHexString(ContextCompat.getColor(ctx, color_id) & 0x00ffffff) + ">" + (underline ? "<u>" + text + "</u>" : text) + "</font>";
    }

    public static void set_text_decoration(AppCompatTextView textView, String text) {
        textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
    }

    public static void set_bottom_sheet(AppCompatActivity appCompatActivity, ConstraintLayout cons_lay, int percentage) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        appCompatActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = (displayMetrics.heightPixels / 100) * percentage;
        cons_lay.setMaxHeight(height);

        BottomSheetBehavior btm_sheet = BottomSheetBehavior.from(cons_lay);
        btm_sheet.setHideable(false);
        btm_sheet.setPeekHeight(0);
        btm_sheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        btm_sheet.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    btm_sheet.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }
}